import 'package:flutter_test/flutter_test.dart';
import 'package:module_common/events/event.dart';
import 'package:module_common/events/event_subscriber.dart';

void main() {
  group('Global Event Test', () {
    test('Should be able to start, listen, and close the subscriber', () {
      GlobalEvent.initializeGlobalEvent();
      dynamic resultData;
      var subscriber = GlobalEventSubscriber('test', (data) => resultData = data);

      GlobalEvent.subscribe(subscriber);
      GlobalEvent.publish('test', 'test');

      expect(resultData, 'test');

      GlobalEvent.unsubscribe(subscriber);
      GlobalEvent.publish('test', 'test2');

      expect(resultData, 'test');
    });
  });
}