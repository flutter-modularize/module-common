import 'package:flutter_test/flutter_test.dart';
import 'package:module_common/events/event_subscriber.dart';

void main() {
  group('Global Event Subscriber Test', () {
    test('Should be counted as a same subscriber', () {
      var subscriber = GlobalEventSubscriber('test', (data) {});
      expect(subscriber, subscriber);
    });

    test('shoud be counted as a different subsciber', () {
      var subscriber1 = GlobalEventSubscriber('test', (data) {});
      var subscriber2 = GlobalEventSubscriber('test', (data) {});
      expect(subscriber1, isNot(subscriber2));
    });
  });
}