import 'package:flutter/material.dart';
import 'package:module_common/constants/color_constant.dart';

class ButtonStyleData {
  final ButtonStyle buttonStyle;
  final TextStyle textStyle;

  const ButtonStyleData({
    required this.buttonStyle,
    required this.textStyle,
  });
}

MaterialStateProperty<T> _stateWrapper<T>(T value) =>
    MaterialStateProperty.all<T>(value);

class ButtonStyleConstant {
  static final primary = ButtonStyleData(
    buttonStyle: ButtonStyle(
      alignment: Alignment.center,
      backgroundColor: _stateWrapper(ColorConstant.primary),
    ),
    textStyle: const TextStyle(color: Colors.white),
  );

  static final secondary = ButtonStyleData(
    buttonStyle: ButtonStyle(
      alignment: Alignment.center,
      backgroundColor: _stateWrapper(ColorConstant.transparent),
      shape: _stateWrapper(RoundedRectangleBorder(
        side: const BorderSide(color: ColorConstant.primary, width: 2),
        borderRadius: BorderRadius.circular(99),
        ),
      ),
    ),
    textStyle: const TextStyle(color: ColorConstant.primary),
  );

  static final danger = ButtonStyleData(
    buttonStyle: ButtonStyle(
      alignment: Alignment.center,
      backgroundColor: _stateWrapper(ColorConstant.transparent),
      shape: _stateWrapper(RoundedRectangleBorder(
        side: const BorderSide(color: ColorConstant.red, width: 2),
        borderRadius: BorderRadius.circular(99),
      ),
      ),
    ),
    textStyle: const TextStyle(color: ColorConstant.red),
  );
}
