import 'package:flutter/src/material/button_style.dart';
import 'package:module_common/widgets/button/button.dart';

enum ButtonStyleEnum {
  primary,
  secondary,
  danger,
}

extension ButtonStyleEnumExtension on ButtonStyleEnum {
  ButtonStyleData get style {
    switch (this) {
      case ButtonStyleEnum.primary:
        return ButtonStyleConstant.primary;
      case ButtonStyleEnum.secondary:
        return ButtonStyleConstant.secondary;
      case ButtonStyleEnum.danger:
        return ButtonStyleConstant.danger;
    }
  }
}