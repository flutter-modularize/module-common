import 'package:flutter/material.dart';
import 'package:module_common/widgets/button/button.dart';

class Button extends StatelessWidget {
  final VoidCallback onPressed;
  final ButtonStyleEnum style;
  final bool disabled;
  final String title;

  const Button({
    super.key,
    required this.onPressed,
    required this.style,
    required this.title,
    this.disabled = false,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: disabled ? null : onPressed,
      style: style.style.buttonStyle,
      child: Text(title, style: style.style.textStyle),
    );
  }
}
