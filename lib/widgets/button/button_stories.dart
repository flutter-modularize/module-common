import 'package:flutter/material.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;
import 'button.dart';

@widgetbook.UseCase(name: 'Primary', type: Button)
Button primaryButton(BuildContext context) {
  return Button(
    onPressed: () {},
    style: ButtonStyleEnum.primary,
    title: 'Primary',
  );
}

@widgetbook.UseCase(name: 'Secondary', type: Button)
Button secondaryButton(BuildContext context) {
  return Button(
    onPressed: () {},
    style: ButtonStyleEnum.secondary,
    title: 'Secondary',
  );
}

@widgetbook.UseCase(name: 'Danger', type: Button)
Button dangerButton(BuildContext context) {
  return Button(
    onPressed: () {},
    style: ButtonStyleEnum.danger,
    title: 'Danger',
  );
}