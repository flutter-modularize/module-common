import 'package:flutter/material.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;
import 'card_container.dart';

@widgetbook.UseCase(name: 'Primary', type: CardContainer)
CardContainer primaryButton(BuildContext context) {
  return const CardContainer(
    child: SizedBox(
      height: 200,
      width: 200,
    ),
  );
}