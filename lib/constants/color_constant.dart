import 'package:flutter/material.dart';

class ColorConstant {
    static const Color primary = Colors.amber;
    static const Color accent = Color(0xFF00BFA5);
    static const Color red = Color(0xFFE53935);
    static const Color transparent = Colors.transparent;
    static const Color white = Colors.white;
}