class LayoutConstant {
    static const double padding = 16.0;
    static const double paddingSmall = 8.0;
    static const double paddingMedium = 16.0;
    static const double paddingLarge = 24.0;

    static const double w_16 = 16;
}