import 'package:event/event.dart';
import 'package:module_common/events/event_subscriber.dart';

class GlobalEvent {
  static late Event<Values<String, dynamic>> _event;
  static final List<GlobalEventSubscriber> _subscribers = [];

  static void initializeGlobalEvent() {
    _event = Event<Values<String, dynamic>>()
      ..subscribe((args) {
        for (var subscriber in _subscribers) {
          if (subscriber.eventName == args?.value1) {
            subscriber.callback(args?.value2);
          }
        }
      });
  }

  static void subscribe(GlobalEventSubscriber subscriber) {
    _subscribers.add(subscriber);
  }

  static void unsubscribe(GlobalEventSubscriber subscriber) {
    _subscribers.remove(subscriber);
  }

  static void publish(String eventName, dynamic data) {
    _event.broadcast(Values<String, dynamic>(eventName, data));
  }
}
