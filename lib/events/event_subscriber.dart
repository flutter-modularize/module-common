class GlobalEventSubscriber {
  final String eventName;
  final Function(dynamic) callback;

  GlobalEventSubscriber(this.eventName, this.callback);
}