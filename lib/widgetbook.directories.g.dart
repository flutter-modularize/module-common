// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: unused_import, prefer_relative_imports, directives_ordering

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AppGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:module_common/widgets/button/button_stories.dart' as _i2;
import 'package:module_common/widgets/card_container/card_container_stories.dart'
    as _i3;
import 'package:widgetbook/widgetbook.dart' as _i1;

final directories = <_i1.WidgetbookNode>[
  _i1.WidgetbookFolder(
    name: 'widgets',
    children: [
      _i1.WidgetbookFolder(
        name: 'button',
        children: [
          _i1.WidgetbookComponent(
            name: 'Button',
            useCases: [
              _i1.WidgetbookUseCase(
                name: 'Danger',
                builder: _i2.dangerButton,
              ),
              _i1.WidgetbookUseCase(
                name: 'Primary',
                builder: _i2.primaryButton,
              ),
              _i1.WidgetbookUseCase(
                name: 'Secondary',
                builder: _i2.secondaryButton,
              ),
            ],
          )
        ],
      ),
      _i1.WidgetbookFolder(
        name: 'card_container',
        children: [
          _i1.WidgetbookComponent(
            name: 'CardContainer',
            useCases: [
              _i1.WidgetbookUseCase(
                name: 'Primary',
                builder: _i3.primaryButton,
              )
            ],
          )
        ],
      ),
    ],
  )
];
